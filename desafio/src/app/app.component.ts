import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private servico: AppService) { }
  meses = [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro'
  ];
  categoria: any = [];
  title = 'desafio';
  lista: any = [];
  listaCompleta: any = [];
  ngOnInit() {

    this.servico.categorias().subscribe(x => {
      if (x) {
        this.categoria = x;
      }
    });

    this.servico.lancamentos().subscribe(x => {
      if (x) {
        this.lista = x;
        this.listaCompleta = x;
      }
    });
  }
  limpar() {
    this.lista = this.listaCompleta;
  }

  adicionar() {

  }

  filtrar(i) {
    this.lista = this.listaCompleta.filter(x => x.mes_lancamento === i);
  }

  filtrarCategoria(i) {
    this.lista = this.listaCompleta.filter(x => x.categoria === i);
  }
}

