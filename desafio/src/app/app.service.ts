import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable(
  { providedIn: 'root' }
)
export class AppService {

  base = environment.baseUrl;
  constructor(protected readonly http: HttpClient) { }

  lancamentos() {
    return this.http.get(this.base + 'lancamentos');
  }

  categorias() {
    return this.http.get(this.base + 'categorias');
  }
}
